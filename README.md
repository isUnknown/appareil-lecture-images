# Basic usage
Launch dev server with :

`php -S 192.168.1.12:8888 -t dist`

For production, better use with a more robust server such as MAMP/WAMP.

On the display device, navigate to through any web browsers : `http://192.168.1.12:8888/display`
On the controller device, scan the display QRCode or navigate to : `http://192.168.1.12:8888/control`

## Run it on a Mac broadcasting its own WiFi network

[source: makeuseof.com](https://www.makeuseof.com/how-to-create-a-secure-ad-hoc-network-in-macos/)

Apple may have removed ad hoc network security from macOS, but, with a nifty work-around, you can replicate this lost feature. That's why the two first commands are made for.

1. In the terminal, run : `sudo networksetup -createnetworkservice AdHoc lo0` (enter your root password if needed)
2. In the terminal, run : `sudo networksetup -setmanual AdHoc 192.168.1.88 255.255.255.255` (enter your root password if needed)
3. Open System Preferences > Sharing.
4. Select Internet Sharing.
5. Click the Share your connection menu and choose AdHoc. 
6. Tick Wi-Fi to enable sharing. 
7. Click Wi-Fi Options. Enter a Network Name and Password. Click OK.
8. Tick Internet Sharing. Click start to confirm
import { defineStore, storeToRefs } from "pinia"
import { ref } from "vue"
import { getData, updateData } from "../assets/js/api"
import { useNiveauStore } from "./niveauStore.js"
import { useNavStore } from "./navStore.js"
import { useMontageStore } from "./montageStore"

export const useAutoRefreshStore = defineStore("AutoRefreshStore", () => {
  const { nav } = storeToRefs(useNavStore())
  const { niveaux } = storeToRefs(useNiveauStore())
  const { montages } = storeToRefs(useMontageStore())

  const isLocked = ref(false)

  function autoRefresh(isAbleToUpdateData = false) {
    setInterval(() => {
      getData().then((data) => {
        if ((!data || !data.nav) && isAbleToUpdateData) {
          const currentApp = eval(nav.value.toLowerCase())
          console.log(
            "autoRefresh then getData, no data.nav, update to : ",
            nav.value
          )
          updateData("nav", currentApp.value).then((data) => {
            console.log("update data from autorefresh.getData : ", data)
          })
        } else {
          nav.value = data.nav
          if (!isLocked.value) {
            updateStore(nav.value, data)
          } else {
            console.log("locked")
          }
        }
      })
    }, 250)
  }

  function updateStore(appName, data) {
    if (appName && typeof appName === "string") {
      appName = appName.toLowerCase()
      const storeName = appName
      const store = eval(storeName)
      if (JSON.stringify(store.value) !== JSON.stringify(data[appName])) {
        console.log("update store ", storeName)
        store.value = data[appName]
      }
    }
  }

  return { autoRefresh, isLocked }
})

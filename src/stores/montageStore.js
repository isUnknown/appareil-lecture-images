import { defineStore, storeToRefs } from "pinia"
import { ref, computed, onMounted, onBeforeMount, onUpdated, watch } from "vue"
import { getData, updateData } from "../assets/js/api"
import { useAutoRefreshStore } from "./autoRefreshStore.js"

export const useMontageStore = defineStore("MontageStore", () => {
  // =========================================== DATA
  const debug = true

  const montages = ref(false)

  function toggleLockAutoRefresh() {
    const { isLocked } = storeToRefs(useAutoRefreshStore())
    isLocked.value = !isLocked.value
  }
  // =========================================== INIT FUNCTIONS
  // getData("montages").then((data) => {
  //   montages.value = data
  // })

  // =========================================== COMPUTED
  const visibleMontage = computed(() => {
    return montages.value.find((montage) => montage.isVisible)
  })
  const visibleItem = computed(() => {
    return visibleMontage.value.items.find((item) => item.isVisible)
  })

  // =========================================== FUNCTIONS
  function update() {
    toggleLockAutoRefresh()
    updateData("montages", montages.value).then((data) => {
      console.log("updateData from montages : ", data)
      toggleLockAutoRefresh()
    })
  }
  function nextItem() {
    let nextVisibleIndex
    visibleMontage.value.items.forEach((item, index) => {
      if (item.isVisible) {
        item.isVisible = false
        nextVisibleIndex =
          visibleMontage.value.items.indexOf(item) + 1 >
          visibleMontage.value.items.length - 1
            ? 0
            : visibleMontage.value.items.indexOf(item) + 1
      }
    })
    visibleMontage.value.items[nextVisibleIndex].isVisible = true

    update()
  }

  function toggleMontage(montage) {
    const isVisible = montage.isVisible
    hideAllMontages()
    if (!isVisible) montage.isVisible = true

    update()
  }

  function hideAllMontages() {
    montages.value = montages.value.map((montage) => {
      montage.isVisible = false
      return montage
    })
  }

  return {
    montages,
    visibleMontage,
    visibleItem,
    nextItem,
    toggleMontage,
  }
})

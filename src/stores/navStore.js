import { defineStore, storeToRefs } from "pinia"
import { ref } from "vue"
import { updateData } from "../assets/js/api"
import { useAutoRefreshStore } from "../stores/autoRefreshStore.js"

export const useNavStore = defineStore("NavStore", () => {
  const nav = ref("Nav")

  function toggleLockAutoRefresh() {
    const { isLocked } = storeToRefs(useAutoRefreshStore())
    isLocked.value = !isLocked.value
  }

  function navigateTo(target) {
    if (nav.value && nav.value !== null && nav.value.length > 0) {
      toggleLockAutoRefresh()
      nav.value = target
      updateData("nav", nav.value)
      // SECOND TIME TO CONFIRM
      setTimeout(() => {
        nav.value = target
        updateData("nav", nav.value).then((data) => {
          toggleLockAutoRefresh()
        })
      }, 100)
    }
  }

  return { nav, navigateTo }
})

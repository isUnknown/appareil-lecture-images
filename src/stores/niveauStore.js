import { defineStore, storeToRefs } from "pinia"
import { ref, computed, onMounted, onBeforeMount, onUpdated, watch } from "vue"
import { getData, updateData } from "../assets/js/api"
import { useAutoRefreshStore } from "./autoRefreshStore.js"

export const useNiveauStore = defineStore("NiveauStore", () => {
  // =========================================== DATA
  const debug = true

  const niveaux = ref(false)

  function toggleLockAutoRefresh() {
    const { isLocked } = storeToRefs(useAutoRefreshStore())
    isLocked.value = !isLocked.value
  }

  // =========================================== HOOKS
  onBeforeMount(() => {
    getData("niveaux").then((data) => {
      niveaux.value = data
    })
  })

  // =========================================== COMPUTED
  const visibleGroup = computed(() => {
    if (!niveaux.value) {
      return false
    } else {
      return niveaux.value.find((group) => group.isVisible)
    }
  })

  // =========================================== FUNCTIONS
  function toggleLayer(groupName, imageIndex) {
    toggleLockAutoRefresh()
    setTimeout(() => {
      const targetGroup = niveaux.value.find(
        (group) => group.name === groupName
      )
      targetGroup.images[imageIndex].isVisible =
        !targetGroup.images[imageIndex].isVisible

      updateData("niveaux", niveaux.value).then((data) => {
        toggleLockAutoRefresh()
      })
    }, 100)
  }

  function prevGroup() {
    const visibleGroupIndex = niveaux.value.indexOf(visibleGroup.value)
    visibleGroup.value.isVisible = false
    if (visibleGroupIndex > 0) {
      niveaux.value[visibleGroupIndex - 1].isVisible = true
    } else {
      niveaux.value[niveaux.value.length - 1].isVisible = true
    }
    toggleLockAutoRefresh()
    updateData("niveaux", niveaux.value).then((data) => {
      toggleLockAutoRefresh()
    })
  }
  function nextGroup() {
    const visibleGroupIndex = niveaux.value.indexOf(visibleGroup.value)
    visibleGroup.value.isVisible = false
    if (visibleGroupIndex < niveaux.value.length - 1) {
      niveaux.value[visibleGroupIndex + 1].isVisible = true
    } else {
      niveaux.value[0].isVisible = true
    }
    toggleLockAutoRefresh()
    updateData("niveaux", niveaux.value).then((data) => {
      toggleLockAutoRefresh()
    })
  }

  return {
    niveaux,
    visibleGroup,
    toggleLayer,
    prevGroup,
    nextGroup,
  }
})

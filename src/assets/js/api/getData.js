import { restoreData } from "./restoreData.js"

export function getData(appName = null) {
  return new Promise((resolve) => {
    const init = {
      method: "POST",
    }

    if (appName) {
      init.body = JSON.stringify({
        appName: appName,
      })
    }

    fetch(import.meta.env.VITE_SERVER_URL + "scripts/get-data.php", init)
      .then((res) => {
        return res.json()
      })
      .then((json) => {
        const now = Math.floor(Date.now() / 1000)
        console.log(json.lastTimestamp)
        console.log(now)
        if (now - json.lastTimestamp > 30) {
          console.log("Restore data because of inactivity : ")
          restoreData()
        }
        resolve(json)
      })
      .catch((error) => {
        console.log("getData error : ", error)
        restoreData()
      })
  })
}

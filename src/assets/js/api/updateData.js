export async function updateData(appName = null, data) {
  const init = {
    method: "POST",
    body: JSON.stringify({
      data: data,
      appName: appName,
    }),
  }

  return new Promise((resolve) => {
    if (window.location.pathname.includes('display')) {
      resolve('No data update from display')
    } else {
      fetch(import.meta.env.VITE_SERVER_URL + "scripts/update-data.php", init)
        .then((res) => res.text())
        .then((data) => {
          resolve(data)
        })
        .catch(err => {
          console.log(err)
        })
    }
  })
}

export async function restoreData(appName = null, data) {
  const init = {
    method: "GET",
  }

  return new Promise((resolve, reject) => {
    try {
      fetch(import.meta.env.VITE_SERVER_URL + "scripts/restore-data.php", init)
        .then((res) => res.text())
        .then((data) => {
          resolve(data)
        })
    } catch (error) {
      reject(error)
    }
  })
}

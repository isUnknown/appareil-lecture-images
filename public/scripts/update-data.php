<?php
header("Access-Control-Allow-Origin: *");

$jsonRequest = file_get_contents("php://input");
$request = json_decode($jsonRequest);

$newJsonString = '';

if ($request->appName) {
  $dataFile = file_get_contents('../data.json');
  $data = json_decode($dataFile);
  $data->{$request->appName} = $request->data;
  
  $data->lastTimestamp = time();
  
  $newJsonString = json_encode($data);
  file_put_contents('../data.json', $newJsonString);
} else {
  // $newJsonString = json_encode($request->data);
}

// file_put_contents('../data.json', $newJsonString);
echo $newJsonString;
<?php 
header("Access-Control-Allow-Origin: *");

$jsonDefaultData = file_get_contents('../data.lock.json');

$defaultData = json_decode($jsonDefaultData);
$defaultData->lastTimestamp = time();

foreach ($defaultData->niveaux as $niveau) {
  foreach ($niveau->images as $key => $image) {
    $isNotMainImage = $key !== (count($niveau->images) - 1);
    if ($isNotMainImage) {
      $dice = rand(0, 10);
      if ($dice <= 5) {
        $image->isVisible = true;
      } else {
        $image->isVisible = false;
      }
    }
  }
}

$jsonShuffledDefaultData = json_encode($defaultData);

file_put_contents('../data.json', $jsonShuffledDefaultData);

echo json_encode('Data shuffled and restored');
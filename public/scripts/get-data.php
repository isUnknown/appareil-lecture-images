<?php
header('Access-Control-Allow-Origin: *');

$jsonRequest = file_get_contents("php://input");
$request = json_decode($jsonRequest);

$serverJsonData = file_get_contents("../data.json");
$serverData = json_decode($serverJsonData);

$response;

if ($request && $request->appName) {
  $response = $serverData->{$request->appName};
} else {
  $response = $serverData;
}

echo json_encode($response);